from pathlib import Path
from radialColorHistogram.color_density import ColorDensity
from radialColorHistogram.radial_split import RadialSplitter
import requests
import joblib
import zmq
import tempfile

def main():
    pathToModel = './model.dat'
    modelFile = Path(pathToModel)
    if not modelFile.exists():
        model = requests.get('https://bitbucket.org/meme-api-workspace/meme-api/raw/c33fbaf2ac1b1025068f3ba3b81bedcd6fdc31c6/models/model.dat')
        modelFile = open(pathToModel, 'wb')
        modelFile.write(model.content)
        modelFile.close()

    print('model was loaded')

    loadedModel = joblib.load(pathToModel)

    pathToScaler = './scaler.dat'
    scalerFile = Path(pathToScaler)
    if not scalerFile.exists():
        scaler = requests.get('https://bitbucket.org/meme-api-workspace/meme-api/raw/c33fbaf2ac1b1025068f3ba3b81bedcd6fdc31c6/models/scaler.dat')
        scalerFile = open(pathToScaler, 'wb')
        scalerFile.write(scaler.content)
        scalerFile.close()

    print('scaler was loaded')

    loadedScaler = joblib.load(pathToScaler)

    rs = RadialSplitter(nrings=1, nqslices=1)
    cd = ColorDensity(splitter=rs, color_model='HSV', n_bins=8, 
                    scaler=loadedScaler, log_transform=True)
    
    mqContext = zmq.Context()
    socket = mqContext.socket(zmq.REP)
    socket.bind("tcp://*:5557")

    print("Started on 5557 port")

    while True:
        url = socket.recv()

        image = requests.get(url.decode('utf-8'))
        tmpFile = tempfile.NamedTemporaryFile(mode='wb')
        tmpFile.write(image.content)
        paths = [tmpFile.name]
        test_image = cd.transform(paths)

        prediction = loadedModel.predict(test_image)
        print(f"prediction: {prediction}")

        socket.send(prediction[0].encode('utf-8'))

if __name__ == "__main__":
    main()