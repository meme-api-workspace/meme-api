import fasttext
import argparse
import pymorphy2
import zmq

def isContainGenLetters(word) -> bool:
  for l in word:
    if l.isupper():
      return True
    else:
      return False

def main():
    print("[MEME-API] Request analyzer")
    parser = argparse.ArgumentParser(description="[MEME-API] Request analyzer")
    parser.add_argument("-m", "--model", default="./request_model.bin", help="Path to request model")
    args = parser.parse_args()

    model = fasttext.load_model(args.model)
    print(f"Model: {args.model}")

    morph = []

    try:
        morph = pymorphy2.MorphAnalyzer()
    except:
        morph = pymorphy2.MorphAnalyzer(path="./pymorphy2_dicts_ru/data")

    def normalizeRequest(rawRequest):
        requestWords = rawRequest.split(" ")
        resultList = []
        for word in requestWords:
            if isContainGenLetters(word):
                resultList.append(word)
            else:
                resultList.append(morph.parse(word)[0].normal_form)
        
        return " ".join(resultList)

    mqContext = zmq.Context()
    socket = mqContext.socket(zmq.REP)
    socket.bind("tcp://*:5556")

    while True:
        request = socket.recv()

        request = normalizeRequest(request.decode('utf-8'))

        label = model.predict(request)[0][0]
        print(label)

        socket.send(label.encode('utf-8'))



if __name__ == "__main__":
    main()