//package com.example.memeApi.repositories;
//
//import com.example.memeApi.model.CategoryRequest;
//import org.springframework.data.mongodb.repository.MongoRepository;
//import org.springframework.stereotype.Repository;
//
//import java.util.Optional;
//
//@Repository
//public interface CategoryRequestsRepository extends MongoRepository<CategoryRequest, Object> {
//    Optional<CategoryRequest> findByCategory(String category);
//}
