//package com.example.memeApi.controller;
//
//import com.example.memeApi.model.CategoryRequest;
//import com.example.memeApi.service.CategoryRequestService;
//import lombok.RequiredArgsConstructor;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@RestController
//@RequiredArgsConstructor
//@RequestMapping("categoryRequest")
//@CrossOrigin(origins = "http://localhost:3000")
//public class CategoryRequestController {
//
//    private final CategoryRequestService categoryRequestService;
//
//    @GetMapping(value = "/{category}")
//    public CategoryRequest get(@PathVariable String category) {
//        return categoryRequestService.findByCategory(category);
//    }
//
//    @GetMapping(value = "/getRequests")
//    public List<CategoryRequest> getAll() {
//        return categoryRequestService.findAll();
//    }
//
//    @PostMapping()
//    public void create(@RequestBody CategoryRequest categoryRequest) {
//        categoryRequestService.addCategoryRequest(categoryRequest);
//    }
//}
