package com.example.memeApi.controller;

import com.example.memeApi.model.Image;
import com.example.memeApi.model.RequestEntity;
import com.example.memeApi.service.SearchEngineService;
import com.example.memeApi.service.SmartModuleService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.var;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("memeApi")
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
public class RequestController {

    private final SmartModuleService smartModuleService;
    private final SearchEngineService searchEngineService;
    private int searchCount = 0;

    @PostMapping(value = "/searchMeme", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> searchMeme(@RequestBody RequestEntity userRequest) throws Exception {
        searchCount++;
        System.out.println("request for searching: " + userRequest.getRequestText() + "\nindex: " + userRequest.getIndex());

        String requestLabel = smartModuleService.getUserRequestLabel(userRequest.getRequestText()).replaceAll("__label__", "");
        List<Image> imageResults = searchEngineService.findImageUrl(userRequest);

        System.out.println("requestLabel: " + requestLabel);
        List<String> labels = new ArrayList<>();
        System.out.println("image labels: ");
        for (Image image : imageResults) {
            String label = smartModuleService.getImageLabel(image);
            System.out.println(label);
            labels.add(label);
        }

        long equal = labels.stream().filter(label -> label.equals(requestLabel)).count();
        System.out.println("equals labels num: " + equal);
        if (equal != 0) {
            System.out.println("count of searches equals: " + searchCount);
            return prepareResponse(requestLabel, imageResults, labels);
        } else {
            if (searchCount < 4) {
                System.out.println("count of searches: " + searchCount);
                userRequest.setIndex(userRequest.getIndex() + 1);
                searchMeme(userRequest);
            } else {
                return prepareResponse(requestLabel, imageResults, labels);
            }
        }

        System.out.println("prepare response empty: ");
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
        responseHeaders.set("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        return ResponseEntity
                .ok()
                .headers(responseHeaders)
                .body(null);
    }

    private ResponseEntity<String> prepareResponse(String requestLabel, List<Image> imageResults, List<String> labels) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String res = mapper.writeValueAsString(imageResults);

        String log = "requestLabel: " + requestLabel + "\nimageUrl's: " + res + "\nimageLabel: " + labels.toString();
        System.out.println(log);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
        responseHeaders.set("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        System.out.println("prepare response with body: " + res);
        return ResponseEntity
                .ok()
                .headers(responseHeaders)
                .body(res);
    }

//    @GetMapping(value = "/getRequest", produces = MediaType.APPLICATION_JSON_VALUE)
//    public String getRequest() {
//        return requestEntity.getRequestText();
//    }

    @GetMapping(value = "/getImage",
            produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> getImage() throws IOException {

        var imgFile = new ClassPathResource("images/shrek2.jpg");
        byte[] bytes = StreamUtils.copyToByteArray(imgFile.getInputStream());

        return ResponseEntity
                .ok()
                .contentType(MediaType.IMAGE_JPEG)
                .body(bytes);
    }

    @PostMapping(value = "/testSendRequest", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> sendRequest() {
        Image shrekImage = new Image("https://sayingimages.com/wp-content/uploads/jim-halpert-alcoholic-meme.jpg");
        String label = smartModuleService.getImageLabel(shrekImage);

        return ResponseEntity
                .ok()
                .body(label);
    }

}
