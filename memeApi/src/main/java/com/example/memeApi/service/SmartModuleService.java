package com.example.memeApi.service;

import com.example.memeApi.model.Image;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Service
public class SmartModuleService {
    private final static String GET_USER_REQUEST_LABEL_URL = "http://localhost:4040/?request={}";
    private final static String GET_IMAGE_LABEL_URL = "http://13.48.68.88:4041";

    private final RestTemplate restTemplate;

    public SmartModuleService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public String getUserRequestLabel(String userRequest) {
        System.out.println("sending userRequest:" + userRequest + "to smartModule");
        String url = GET_USER_REQUEST_LABEL_URL.replace("{}", userRequest.replace(' ', '_'));

        ResponseEntity<String> response = this.restTemplate.getForEntity(url, String.class, 1);

        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        } else {
            return null;
        }
    }

    public String getImageLabel(Image image) {
        System.out.println("sending image url:" + image.getUrl() + "to smartModule");

        // create headers
        HttpHeaders headers = new HttpHeaders();
        // set `content-type` header
        headers.setContentType(MediaType.APPLICATION_JSON);
        // set `accept` header
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        // build the request
        HttpEntity<Image> entity = new HttpEntity<>(image, headers);

        // send POST request
        ResponseEntity<String> response = this.restTemplate.postForEntity(GET_IMAGE_LABEL_URL, entity, String.class);

        // check response status code
        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        } else {
            return null;
        }
    }

}
