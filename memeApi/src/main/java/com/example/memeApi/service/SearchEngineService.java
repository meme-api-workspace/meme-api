package com.example.memeApi.service;

import com.example.memeApi.model.Image;
import com.example.memeApi.model.RequestEntity;
import com.example.memeApi.model.SearchResults;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.stereotype.Service;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;

@Service
public class SearchEngineService {
    private static final String subscriptionKey = "f739bf967e0f4b8d8f8d44c06e4659fc";
    private static String host = "https://api.bing.microsoft.com";
    private static String path = "/v7.0/images/search";
    private SearchResults results;
    private HttpsURLConnection connection;
    private InputStream stream;

    public List<Image> findImageUrl(RequestEntity requestEntity) throws Exception {
        buildConnection(buildUrl(requestEntity.getRequestText() + "meme"));
        getResponseResult();
        extractBingHeaders();
        return getResults(requestEntity.getIndex());
    }

    private URL buildUrl(String requestText) throws Exception {
        return new URL(host + path + "?q=" + URLEncoder.encode(requestText, "UTF-8"));
    }

    private void buildConnection(URL url) throws IOException {
        this.connection = (HttpsURLConnection) url.openConnection();
        this.connection.setRequestProperty("Ocp-Apim-Subscription-Key", subscriptionKey);
    }

    private void getResponseResult() throws IOException {
        // receive JSON body
        this.stream = connection.getInputStream();
        String response = new Scanner(stream).useDelimiter("\\A").next();
        // construct result object for return
        this.results = new SearchResults(new HashMap<String, String>(), response);
    }


    private void extractBingHeaders() {
        // extract Bing-related HTTP headers
        Map<String, List<String>> headers = this.connection.getHeaderFields();
        for (String header : headers.keySet()) {
            if (header == null) continue;      // may have null key
            if (header.startsWith("BingAPIs-") || header.startsWith("X-MSEdge-")) {
                this.results.relevantHeaders.put(header, headers.get(header).get(0));
            }
        }

    }

    private List<Image> getResults(Integer index) throws IOException {
        System.out.println("the index from which the search starts: " + index);
        this.stream.close();
        JsonObject json = JsonParser.parseString(this.results.jsonResponse).getAsJsonObject();
        //get the first image result from the JSON object, along with the total
        JsonArray allResults = json.getAsJsonArray("value");
        if (allResults.size() == 0) {
            return new ArrayList<>();
        }

        List<Image> results = new ArrayList<>();
        int newIndex = index * 5;

        for (int i = newIndex; i < newIndex + 5; i++) {
            JsonObject res = (JsonObject) allResults.get(i);
            results.add(new Image(res.get("thumbnailUrl").getAsString()));
        }

        return results;
    }
}
