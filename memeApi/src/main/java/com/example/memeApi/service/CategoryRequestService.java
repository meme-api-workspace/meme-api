//package com.example.memeApi.service;
//
//import com.example.memeApi.model.CategoryRequest;
//import com.example.memeApi.repositories.CategoryRequestsRepository;
//import lombok.RequiredArgsConstructor;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//
//@Service
//@RequiredArgsConstructor
//public class CategoryRequestService {
//    private final CategoryRequestsRepository categoryRequestsRepository;
//
//    public void addCategoryRequest(CategoryRequest requests) {
//        categoryRequestsRepository.save(requests);
//    }
//
//    public CategoryRequest findByCategory(String category) {
//        return categoryRequestsRepository.findByCategory(category).orElseThrow(() -> new RuntimeException("Category not found"));
//    }
//
//    public List<CategoryRequest> findAll() {
//        return categoryRequestsRepository.findAll();
//    }
//}
