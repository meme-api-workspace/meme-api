package com.example.memeApi.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.HashMap;

@Data
@AllArgsConstructor
public class SearchResults {
        public HashMap<String, String> relevantHeaders;
        public String jsonResponse;
}
