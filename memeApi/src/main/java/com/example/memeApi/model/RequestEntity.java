package com.example.memeApi.model;

import lombok.Data;

@Data
public class RequestEntity {
    String requestText;
    Integer index;
}
