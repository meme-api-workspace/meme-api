package com.example.memeApi.model;

public class PictureEntity {
    private String pictureLink;

    public String getPictureLink() {
        return pictureLink;
    }

    public void setPictureLink(String pictureLink) {
        this.pictureLink = pictureLink;
    }
}
